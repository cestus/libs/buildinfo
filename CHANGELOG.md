
# CHANGELOG

This CHANGELOG is a format conforming to [keep-a-changelog](https://github.com/olivierlacan/keep-a-changelog). 
It is generated with git-chglog -o CHANGELOG.md


<a name="v0.0.1"></a>
## v0.0.1

### Feat

* add buildinfo package

